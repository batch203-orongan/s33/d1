// [SECTION] JavaScript Synchronous vs Asychronous

//  JavaScript is by default is synchronous meaning that it only executes on statement at a time.

console.log("Hello World!");
// conosle.log("Hello World!");
console.log("Good Bye!");
// Code blocking - waiting for the specefic statement to finish before executing the next statement.
// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// }

console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// [SECTION] Getting all posts

// The Fetch API that allows us to asychronously request for a resources (data).
	// "fetch()" method in JavaScript is used to request to the server and load information on the webpages.
	// Syntax:
		// fetch ("apiURL") 
		// A "promise" is an object that represents the eventual completion (or failure of an asychronous function and its's resulting value.)
			// A promise may be in one of 3 possible satates: fulfilled, rejected, or pending.

			/*
				pending: initial states, neither fulfilled nor rejected response.
					fulfilled: operation was completed
					rejected: operation failed.

					Syntax:
					fetch("apiURL")
					.then((response) => {})

			*/
	fetch("https://jsonplaceholder.typicode.com/posts")
	// The ".then()" method captures the response object and returns another promise which will be either "resolved" or "rejected".
	// .then(response => console.log(response.status));

	// "json()" method will convert JSON format to a javascript object
	.then(response => response.json())
	// we can now manipulate or use the converted response in our code.
	.then(json => console.log(json));
	// We can use array methods to display each post title.
	//.then(json => {
	// 	json.forEach(post => console.log(post.title))
	// });

	// the "async" and "await" keyword to achieve asynchronous code.

	async function fetchData(){
		let result = await(fetch("https://jsonplaceholder.typicode.com/posts"));
		// Returned the "Response" object
		console.log(result)

		let json = await result.json();

		console.log(json);
	};

	fetchData();

	// [SECTION] Getting a specific post
	fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Creating a post

	/*
		Syntax:
			 fetch("apiURL", options)
			 .then((response) => {})
			 .then((response) => {})
	*/

	fetch("https://jsonplaceholder.typicode.com/posts",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					title: "New Post",
					body: "Hello World!",
					userId: 1
				})
			}
		)
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post

	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "updated post",
			body: "Hello again",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Updating a post using PATCH method
	// PUT vs PATCH
		// Patch is used to update a single/several properties.
		// PUT is used to upodate whole document
	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			title: "Corrected post title"
		})
	})
	.then(response => response.json())
	.then(json => console.log(json));

	// [SECTION] Delete a post

	fetch("https://jsonplaceholder.typicode.com/posts/1", {method: "DELETE"})
	
	.then(response => response.json())
	.then(json => console.log(json));